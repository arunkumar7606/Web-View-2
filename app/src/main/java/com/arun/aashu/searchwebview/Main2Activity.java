package com.arun.aashu.searchwebview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    WebView wv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        wv = findViewById(R.id.mywebview);

        // wv.getSettings().setJavaScriptEnabled(true);
        //or
        WebSettings ws = wv.getSettings();
        ws.setJavaScriptEnabled(true);


        Bundle b = getIntent().getExtras();
        wv.setWebViewClient(new MyWebClient());
        String url = b.getString("ok");
//        String url="http://www.google.com";
        wv.loadUrl(url);

    }


    class MyWebClient extends WebViewClient {

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            Toast.makeText(Main2Activity.this, description+failingUrl+String.valueOf(errorCode), Toast.LENGTH_SHORT).show();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }
    }

}